import org.junit.Before;
import org.openqa.selenium.WebDriver;
import utils.config;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.timeout;
import static com.codeborne.selenide.Selenide.open;

public class baseTest {

    private final String url = config.getProperty("baseUrl");
    private final String browserConfig = config.getProperty("driver");
    private final int timeoutConfig = Integer.parseInt(config.getProperty("timeout"));

    WebDriver driver;

    @Before
    public void preConfig() {
        System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");

        com.codeborne.selenide.Configuration.browser = browserConfig;
        timeout = timeoutConfig;
        baseUrl = url;
        open("");
    }
}