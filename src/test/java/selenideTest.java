import org.junit.Test;
import pages.rozetkaMain;
import java.io.FileNotFoundException;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class selenideTest extends baseTest {

    @Test
    public void washPowders() throws FileNotFoundException {
        rozetkaMain mainpage = new rozetkaMain(getWebDriver());
        //tovary dlia doma
        mainpage.homeGoodsOpen();
        //bytovaia himia
        mainpage.chemicalsOpen();
        //sredstva dlia stirki
        mainpage.forLaunderOpen();
        //stir poroshki
        mainpage.washPowderOpen();
        //copy list to file
        mainpage.findWashPowderList();
    }
}
