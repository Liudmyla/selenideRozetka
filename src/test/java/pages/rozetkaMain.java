package pages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.FileNotFoundException;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class rozetkaMain extends basePage {

    //Tovary Dlia Doma link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/tovary-dlya-doma/c2394287/']")
        private WebElement homeGoods;

    //Bytova himia link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/bytovaya-himiya/c4429255/']")
        private WebElement chemicals;

    //Bytova himia title
    @FindBy(xpath = "//*[@class='pab-h1']")
        private WebElement chemicalsTitle;

    //Sredstva dlia stirki link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/sredstva-dlya-stirki/c4625084/']")
        private WebElement forLaunder;

    //Stiralnye poroshki link
    @FindBy(xpath = "//*[@href='http://rozetka.com.ua/sredstva-dlya-stirki/c4625084/filter/tip-sredstva-71899=301219/']")
        private WebElement washPowder;

    //Stiralnye poroshki title
    @FindBy(xpath = "//h1[@itemprop = 'name']")
        private WebElement washPowderTitle;

    //List of wash powder names and prices
    @FindBy(xpath="//*[@class='g-i-tile-i-title clearfix']/a | //div[@class='g-price-uah']")
      private List<WebElement> washPowderList;

    public rozetkaMain(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void homeGoodsOpen() {
        $(homeGoods).click();
    }

    public void chemicalsOpen() {
        $(chemicals).click();
        Assert.assertEquals("Incorrect Chemicals page opened", $(chemicalsTitle).getText(), "Бытовая химия");
    }

    public void forLaunderOpen() {
        $(forLaunder).click();
    }

    public void washPowderOpen() {
        $(washPowder).click();
        Assert.assertEquals("Incorrect WashPowder page opened", $(washPowderTitle).getText(), "Стиральные порошки");
    }

    public void findWashPowderList() throws FileNotFoundException {
        basePage.findList("washPowderList.txt", washPowderList);
    }
}
